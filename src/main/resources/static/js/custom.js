$(document).ready(function() {
document.getElementById("saleryPerHourMin").style.position = "absolute"
document.getElementById("saleryPerHourMin").style.right = "10px"
document.getElementById("saleryPerHourMin").style.width = "100px"
document.getElementById("saleryPerHourMax").style.width = "100px"
document.getElementById("numberOfStudentsMin").style.position = "absolute"
document.getElementById("numberOfStudentsMin").style.right = "10px"
document.getElementById("numberOfStudentsMin").style.width = "100px"
document.getElementById("numberOfStudentsMax").style.width = "100px"
document.getElementById("dateOfStartMin").style.position = "absolute"
document.getElementById("dateOfStartMin").style.right = "10px"
});
// sidebar click
$(document).ready(function() {
	var pathname = window.location.pathname;
	$(".sidebar-wrapper li.active").attr("class", "");
	switch (pathname) {
	case "/home":
		$('.sidebar-wrapper li#jobAds').attr("class", "active");
		break;
	case "/jobAd/list":
		$('.sidebar-wrapper li#jobAdControl').attr("class", "active");
		break;
	case "/student/list":
		$('.sidebar-wrapper li#students').attr("class", "active");
		break;
	case "/employer/list":
		$('.sidebar-wrapper li#employers').attr("class", "active");
		break;
	case "/registration":
		$('.sidebar-wrapper li#registration').attr("class", "active");
		break;
	case "/login":
		$('.sidebar-wrapper li#login').attr("class", "active");
		break;
	case "/profile":
		$('.sidebar-wrapper li#myProfile').attr("class", "active");
		break;
	case "/login?logout":
		$('.sidebar-wrapper li#logout').attr("class", "active");
		break;
	default:

	}

});

// home page prvi element aktivan
$(document).ready(function() {
	$('#list-tab a:first-child').tab('show');
});

// prikaz registacije student/poslodavac
$(document).ready(function() {
	$("input[name$='regType']").click(function() {

		var regType = $(this).val();

		$("div.desc").hide();

		if (regType == "employer") {
			$(employerForm).show();
		} else if (regType == "student") {
			$(studentForm).show();
		}

	});
});

$(document).ready(function() {
	$('.filterBtn').on('click', function(event) {
		$('.myForm #id').val("asdas");
	});
});
function DisableNullFields() {
	$('input[type=hidden]').each(function(i) {
		var $input = $(this);
		if ($input.val() == '')
			$input.attr('disabled', 'disabled');
	});
}
function disableEmptyInputs(form) {
	var controls = form.elements;
	for (var i = 0, iLen = controls.length; i < iLen; i++) {
		controls[i].disabled = controls[i].value == '';
	}
}

$(document).ready(function() {
	$("#myBtn").click(function() {
		$("#myModal").modal();
	});
});

$(function() {
	$('#myList a:last-child').tab('show')
})
// profilna slika
$(document).ready(function() {

	var readURL = function(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('.avatar').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}

	$(".file-upload").on('change', function() {
		readURL(this);
	});
});

function add(ths, sno) {
	for (var i = 1; i <= 5; i++) {
		var cur = document.getElementById("star" + i)
		cur.className = "far fa-star"
	}
	for (var i = 1; i <= sno; i++) {
		var cur = document.getElementById("star" + i)
		if (cur.className == "far fa-star") {
			cur.className = "fas fa-star"
		}
	}
	var grade = document.getElementById("grade");
	grade.value = sno;
}
function modalAddFormStudentId(studentId, jobAdId) {
	$("form input#studentId").attr("value", studentId);
	$("form input#jobAdId").attr("value", jobAdId);
}