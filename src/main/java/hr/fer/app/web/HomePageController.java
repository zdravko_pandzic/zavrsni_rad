package hr.fer.app.web;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import hr.fer.app.model.City;
import hr.fer.app.model.Employer;
import hr.fer.app.model.JobAd;
import hr.fer.app.model.Student;
import hr.fer.app.service.CityService;
import hr.fer.app.service.EmployerService;
import hr.fer.app.service.JobAdService;
import hr.fer.app.service.JobCategoryService;
import hr.fer.app.service.StudentService;

@Controller
public class HomePageController {
	private static final String DATE_PATTERN = "yyyy-MM-dd";
	private static final int MAX_ADS_PER_PAGE=4;
	@Autowired
	JobAdService jobAdService;
	
	@Autowired
	StudentService studentService;
	@Autowired
	EmployerService employerService;
	@Autowired
	CityService cityService;
	@Autowired
	JobCategoryService jobCategoryService;
	
	@GetMapping("/home")
	public String home(
			Model theModel, 
			@RequestParam(defaultValue = "0") int page,
			@RequestParam(value = "saleryPerHourMax", required = false) Optional<String> saleryPerHourMax,
			@RequestParam(value = "saleryPerHourMin", required = false) Optional<String> saleryPerHourMin,
			@RequestParam(value = "numberOfStudentsMin", required = false) Optional<String> numberOfStudentsMin,
			@RequestParam(value = "numberOfStudentsMax", required = false) Optional<String> numberOfStudentsMax,
			@RequestParam(value = "dateOfStartMin", required = false) @DateTimeFormat(pattern = DATE_PATTERN) Optional<Date> dateOfStartMin,
			@RequestParam(value = "dateOfStartMax", required = false) @DateTimeFormat(pattern = DATE_PATTERN) Optional<Date> dateOfStartMax,
			@RequestParam(value = "city", required = false) Optional<String> city,
			@RequestParam(value = "jobCategory", required = false) Optional<String> jobCategory,
			@RequestParam(value = "emergencyAd", required = false) Optional<String> emergencyAd,
			@RequestParam(value = "sort", required = false) Optional<String> sortBy,
			Pageable pageable) {

		Pageable pageRequest;
		
		String sort=sortBy.isPresent()?sortBy.get():"";
		switch (sort) {
			case "1":
				pageRequest=PageRequest.of(page, MAX_ADS_PER_PAGE, Sort.DEFAULT_DIRECTION.DESC, "dateOfPosting");			
				break;
			case "2":
				pageRequest=PageRequest.of(page, MAX_ADS_PER_PAGE, Sort.DEFAULT_DIRECTION.ASC, "dateOfPosting");
				break;
			case "3":
				pageRequest=PageRequest.of(page, MAX_ADS_PER_PAGE, Sort.DEFAULT_DIRECTION.DESC, "saleryPerHour");
				break;
	
			default:
				pageRequest=PageRequest.of(page, MAX_ADS_PER_PAGE, Sort.DEFAULT_DIRECTION.ASC, "id");
				break;
		}
		
		
		LocalDate localDateOfStartMin=null;
		LocalDate localDateOfStartMax =null;
		if(dateOfStartMin.isPresent()) {
			localDateOfStartMin = dateOfStartMin.get().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		}
		if(dateOfStartMax.isPresent()) {
			localDateOfStartMax = dateOfStartMax.get().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();		
		}	
		
		Page<JobAd> list=jobAdService.findAll(saleryPerHourMax, saleryPerHourMin, numberOfStudentsMin,numberOfStudentsMax,localDateOfStartMin,localDateOfStartMax,city,jobCategory,emergencyAd,true, pageRequest);
		//Page<JobAd> list=jobAdService.findAll(pageRequest,saleryPerHour.orElse(""));

		theModel.addAttribute("jobAds",list);
		theModel.addAttribute("currentPage", page);
		theModel.addAttribute("cities", cityService.findAll());
		theModel.addAttribute("jobCategories", jobCategoryService.findAll());
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String email = auth.getName();
		theModel.addAttribute("userEmail", email);
		
		
		
		return "home";
	}
	
	
	@GetMapping("/profile")
	public String profile(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String theEmail = auth.getName();
		Student student=studentService.findByEmail(theEmail);
		if(Objects.nonNull(student)) {
			return "redirect:/student/profile?studentId="+student.getId();
//			model.addAttribute("student", student);
//			model.addAttribute("jobAds", student.getFinishedJob());
//			return "student/student-profile";
		}

		else {
			Employer employer = employerService.findByEmail(theEmail);
			return "redirect:/employer/profile?employerId="+employer.getId();
//			model.addAttribute("employer", employer);
//			
//			return "employer/employer-profile";
		}
	}
	


	@ResponseBody
	@GetMapping("/jobs")
	public List<JobAd> bezze(
			@RequestParam(value = "salery_per_hour", required = false) Integer salery_per_hour,
			@RequestParam(value = "date_of_start", required = false) String date_of_start,
			@RequestParam(value = "city", required = false) City city) {
		
		//return JobAdService.function(salery_per_hour,date_of_start,city,pageable);
		//model.addAttribute("id", id);
		
		jobAdService.save(new JobAd());
		jobAdService.save(new JobAd());
		List<JobAd> list = new ArrayList<>(jobAdService.findAll());
		return list;
	}
	
	@GetMapping("/apply")
	public String apply(@RequestParam(value="jobAdId") long jobAdId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String email = auth.getName();
		
		Student student = studentService.findByEmail(email);

		JobAd jobAd = jobAdService.findById(jobAdId);

		jobAdService.addStudentToJobAdApply(jobAdId,student.getId());
		return "redirect:/home";
	}
	
	
	
}
