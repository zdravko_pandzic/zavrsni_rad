package hr.fer.app.web;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import hr.fer.app.model.Employer;
import hr.fer.app.model.JobAd;
import hr.fer.app.model.Review;
import hr.fer.app.model.Student;
import hr.fer.app.service.StudentService;

@RequestMapping("student")
@Controller
public class StudentController {

	@Autowired
	StudentService studentService;

	@GetMapping("/list")
	public String studentList(Model model) {

		List<Student> students = studentService.findAll();
		model.addAttribute("students", students);

		return "student/student-list";
	}

	@GetMapping("/profile")
	public String employerListId(Model model, @RequestParam("studentId") long theId) {

		Student student = studentService.findById(theId);
		model.addAttribute("student", student);
		
		List<Review> reviews = student.getStudentReviews().stream().filter(r ->r.getWhoWroteIt().equals("employer")).collect(Collectors.toList());
		model.addAttribute("reviews", reviews);
		
		if (reviews.size() > 0) {
			double rating = 0;
			for (Review review : reviews) {
				rating += review.getGrade();
			}
			rating /= reviews.size();
			model.addAttribute("rating", String.format("%.2f", rating));
		}else {
			model.addAttribute("rating", "-");
		}
		
		model.addAttribute("jobAds", student.getFinishedJob());


		return "student/student-profile";
	}

	@PostMapping("/save")
	public String save(@ModelAttribute("jobAd") Student student) {

		studentService.save(student);

		return "redirect:/student/list";
	}

	@GetMapping("/delete")
	public String delete(@RequestParam("studentId") long theId) {

		studentService.deleteById(theId);

		return "redirect:/student/list";
	}

}
