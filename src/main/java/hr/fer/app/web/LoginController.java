package hr.fer.app.web;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import hr.fer.app.dao.StudentRepository;
import hr.fer.app.model.City;
import hr.fer.app.model.Employer;
import hr.fer.app.model.Student;
import hr.fer.app.service.CityService;
import hr.fer.app.service.EmployerService;
import hr.fer.app.service.StudentService;

@Controller
public class LoginController {

	@Autowired
	StudentService studentService;
	@Autowired
	EmployerService employerService;

	@Autowired
	protected AuthenticationManager authenticationManager;

	@Autowired
	private StudentRepository studentRepository;

	@Autowired
	EntityManager entityManager;

	@GetMapping("/login")
	public String login() {
		return "login-registration/login";
	}

	@GetMapping("/registration")
	public String registration(Model model) {
		model.addAttribute("student", new Student());
		model.addAttribute("employer", new Employer());
		model.addAttribute("kao", new String());
		return "login-registration/registration";
	}

	@PostMapping("/registration-student")
	public String registerStudentAccount(@Valid @ModelAttribute("student") Student student, BindingResult result,
			HttpServletRequest request) {

		if (emailExists(student.getEmail())) {
			result.rejectValue("email", null, "There is already an account registered with that email");
		}
		if (result.hasErrors()) {
			return "registration";
		}

		studentService.save(student);
		authenticateUserAndSetSession(student.getEmail(), student.getPassword(), request);
		return "redirect:/home";
	}

	@PostMapping("/registration-employer")
	public String registerStudentAccount(@Valid @ModelAttribute("employer") Employer employer, BindingResult result,
			HttpServletRequest request) {
		if (emailExists(employer.getEmail())) {
			result.rejectValue("email", null, "There is already an account registered with that email");
		}
		if (result.hasErrors()) {
			return "registration";
		}
		emailExists(employer.getEmail());

		employerService.save(employer);

		authenticateUserAndSetSession(employer.getEmail(), employer.getPassword(), request);
		return "redirect:/home";
	}

	@ResponseBody
	@GetMapping("/user")
	public String user() {
		return "user";
	}

	@ResponseBody
	@GetMapping("/admin")
	public String admin() {
		return "admin";
	}

	private void authenticateUserAndSetSession(String username, String password, HttpServletRequest request) {
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);

		request.getSession();

		token.setDetails(new WebAuthenticationDetails(request));
		Authentication authenticatedUser = authenticationManager.authenticate(token);

		SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
	}

	private boolean emailExists(String email) {
		if (studentService.findByEmail(email) == null || employerService.findByEmail(email) == null)
			return false;
		return true;

	}
}
