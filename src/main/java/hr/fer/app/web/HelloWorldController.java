package hr.fer.app.web;

import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class HelloWorldController {


	@GetMapping("/time")
	public String sayHello() {
		return "hello world, time on server is " + LocalDateTime.now();
	}

	
	@RequestMapping("/showForm")
	public String showForm() {
		return "helloworld/helloworld-form";
	}
	
	@RequestMapping("/processForm")
	public String processForm(Model model){
//		model.addAttribute("studentName", studentName);
//		model.addAttribute("prezime", prezime);
		return "helloworld/helloworld";
	}
	
	//access data from model
	@RequestMapping("/form1")
	public String letsShoutDude(@RequestParam("studentName") String name,Model model) {

		//dphvacanje parametra anotacijom
		//public String letsShoutDude(HttpServletRequest request, Model model,@RequestParam String studentName) {
		//String name = (String) request.getParameter("studentName");
		
		name = name.toUpperCase();
		model.addAttribute("message","Yo! " + name );
		return "helloworld/helloworldAccessDataFromModel";
	}
}
