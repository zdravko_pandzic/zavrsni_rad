package hr.fer.app.web.vjezbanje;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.app.model.City;
import hr.fer.app.model.Student;
import hr.fer.app.service.CityService;
import hr.fer.app.service.StudentService;

@RestController
@RequestMapping("daj")
public class RestVj {
	
	@Autowired
	private CityService gradRepo;
	@Autowired
	private StudentService studentService;
	
	@GetMapping("/gradove")
	public List<City> grad(){
		return gradRepo.findAll();
	}
	
	@GetMapping("/studente")
	public List<Student> studetne(){
		return studentService.findAll();
	}
	
	
}
