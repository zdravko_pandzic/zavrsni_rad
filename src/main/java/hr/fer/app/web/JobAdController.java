package hr.fer.app.web;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import hr.fer.app.model.JobAd;
import hr.fer.app.service.CityService;
import hr.fer.app.service.EmployerService;
import hr.fer.app.service.JobAdService;
import hr.fer.app.service.JobCategoryService;

@Controller
@RequestMapping("jobAd")
public class JobAdController {

	private static final String DATE_PATTERN = "yyyy-MM-dd";

	@Autowired
	private JobAdService jobAdService;
	@Autowired
	private EmployerService employerService;
	@Autowired
	private CityService cityService;
	@Autowired
	private JobCategoryService jobCategoryService;

	@GetMapping("/list")
	public String list(Model model) {
		model.addAttribute("jobAds", jobAdService.findAll());
		return "job-ads/job-ad-list";
	}
	
	@GetMapping("/list/{id}")
	public String list(@PathVariable long id,Model model) {
		model.addAttribute("jobAd", jobAdService.findById(id));
		return "job-ads/job-ad-details";
	}

	@GetMapping("showFormForAdd")
	public String showFormForAdd(Model theModel) {

		JobAd jobAd = new JobAd();

		theModel.addAttribute("jobAd", jobAd);
		theModel.addAttribute("cities", cityService.findAll());
		theModel.addAttribute("jobCategories", jobCategoryService.findAll());
		return "job-ads/job-ad-form";
	}

	@GetMapping("showFormForUpdate")
	public String showFromForUpdate(@RequestParam("jobAdId") long theId, Model theModel) {

		JobAd jobAd = jobAdService.findById(theId);
		theModel.addAttribute("jobAd", jobAd);
		theModel.addAttribute("cities", cityService.findAll());
		theModel.addAttribute("jobCategories", jobCategoryService.findAll());

		return "job-ads/job-ad-form";

	}

	@PostMapping("/save")
	public String save(@ModelAttribute("jobAd") JobAd jobAd) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String email = auth.getName();
		jobAd.setEmployer(employerService.findByEmail(email));
		jobAd.setActive(true);

		LocalDate today = LocalDate.now();
		jobAd.setDateOfPosting(today);

		jobAdService.save(jobAd);

		return "redirect:/home";
	}

	@GetMapping("/delete")
	public String delete(@RequestParam("jobAdId") long theId) {

		jobAdService.deleteById(theId);

		return "redirect:/jobAd/list";
	}

	@GetMapping("/deactivate")
	public String deactivate(@RequestParam("jobAdId") long theId) {

		JobAd jobAd = jobAdService.findById(theId);
		jobAd.setActive(false);
		jobAdService.save(jobAd);

		return "redirect:/profile";
	}

	@GetMapping("/updateJobAdFinished")
	public String update(@RequestParam("jobAdId") long jobAdId,@RequestParam("studentId") long studentId) {
		jobAdService.addStudentToJobAdFinished(jobAdId, studentId);
		return "redirect:/home";
	}

}
