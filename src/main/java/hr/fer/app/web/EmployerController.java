package hr.fer.app.web;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import hr.fer.app.model.Employer;
import hr.fer.app.model.JobAd;
import hr.fer.app.model.Review;
import hr.fer.app.model.Student;
import hr.fer.app.service.EmployerService;

@RequestMapping("employer")
@Controller
public class EmployerController {

	@Autowired
	EmployerService employerService;

	@GetMapping("/list")
	public String employerList(Model model) {

		List<Employer> employers = employerService.findAll();

		model.addAttribute("employers", employers);

		return "employer/employer-list";
	}

	@GetMapping("/profile")
	public String employerListId(Model model, @RequestParam("employerId") long theId) {

		Employer employer = employerService.findById(theId);
		model.addAttribute("employer", employer);

		List<Review> reviews = employer.getEmployerReviews().stream().filter(r -> r.getWhoWroteIt().equals("student"))
				.collect(Collectors.toList());
		model.addAttribute("reviews", reviews);
		if (reviews.size() > 0) {

			double rating = 0;
			for (Review review : reviews) {
				rating += review.getGrade();
			}
			rating /= reviews.size();
			model.addAttribute("rating", String.format("%.2f", rating));
		}else {
			model.addAttribute("rating", "-");
		}

		List<JobAd> activeJobAds = employer.getJobAds().stream().filter(jobAd -> jobAd.isActive() == true)
				.collect(Collectors.toList());
		model.addAttribute("activeJobAds", activeJobAds);

		List<JobAd> jobAdsFinished = employer.getJobAds().stream().filter(jobAd -> jobAd.isActive() == false)
				.collect(Collectors.toList());
		model.addAttribute("jobAdsFinished", jobAdsFinished);

		return "employer/employer-profile";
	}

	@PostMapping("/save")
	public String save(@ModelAttribute("jobAd") Employer employer) {

		employerService.save(employer);

		return "redirect:/employer/list";
	}

	@GetMapping("/delete")
	public String delete(@RequestParam("employerId") long theId) {

		employerService.deleteById(theId);

		return "redirect:/employer/list";
	}
}
