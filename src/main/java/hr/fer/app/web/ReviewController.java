package hr.fer.app.web;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import hr.fer.app.dao.ReviewRepository;
import hr.fer.app.model.Employer;
import hr.fer.app.model.JobAd;
import hr.fer.app.model.Review;
import hr.fer.app.model.Student;
import hr.fer.app.service.EmployerService;
import hr.fer.app.service.JobAdService;
import hr.fer.app.service.ReviewService;
import hr.fer.app.service.StudentService;

@Controller
public class ReviewController {
	@Autowired
	JobAdService jobAdService;

	@Autowired
	StudentService studentService;
	@Autowired
	EmployerService employerService;

	@Autowired
	ReviewService reviewService;

	@PostMapping("review/save")
	public String saveReview(@RequestParam(value = "review") String reviewText,
			@RequestParam(value = "grade") int grade,
			@RequestParam(value = "studentId") int studentId,
			@RequestParam(value = "jobAdId") long jobAdId) {
		JobAd jobAd = jobAdService.findById(jobAdId);
		Employer employer = jobAd.getEmployer();

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String mail = auth.getName();

		String type;
		Student student = studentService.findByEmail(mail);
		
		if(Objects.nonNull(student)) {
			type="student";
		}

		else {
			type="employer";
			student=studentService.findById(studentId);
		}
		Review review = new Review(0, employer,student, jobAd, grade, reviewText,type);
		reviewService.save(review);
		return "redirect:/profile";
	}


}
