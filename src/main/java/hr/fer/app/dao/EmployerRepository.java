package hr.fer.app.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import hr.fer.app.model.Employer;

public interface EmployerRepository extends JpaRepository<Employer, Long> {
	
	@Modifying
    @Query(value = "insert into users (USERNAME,PASSWORD,ENABLED) values (:email,:password,TRUE)", nativeQuery = true)
    @Transactional
    void saveInUserTable(@Param("email") String insertLink, @Param("password") String password);

	@Modifying
    @Query(value = "insert into AUTHORITIES (USERNAME, AUTHORITY)  values (:email,'ROLE_EMPLOYER')", nativeQuery = true)
    @Transactional
    void saveInAuthoritiesTable(@Param("email") String insertLink);
	
	public Employer findByEmail(String theEmail);

}
