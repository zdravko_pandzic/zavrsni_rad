package hr.fer.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.fer.app.model.JobCategory;

public interface JobCategoryRepository extends JpaRepository<JobCategory, Long> {

}
