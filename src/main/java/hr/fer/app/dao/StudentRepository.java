package hr.fer.app.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import hr.fer.app.model.Student;

@RepositoryRestResource(path="students")
public interface StudentRepository extends JpaRepository<Student, Long> {
	
	@Modifying
    @Query(value = "insert into USERS (USERNAME,PASSWORD,ENABLED) values (:email,:password,TRUE)", nativeQuery = true)
    @Transactional
    void saveInUserTable(@Param("email") String insertLink, @Param("password") String password);

	@Modifying
    @Query(value = "insert into AUTHORITIES (USERNAME, AUTHORITY)  values (:email,'ROLE_STUDENT')", nativeQuery = true)
    @Transactional
    void saveInAuthoritiesTable(@Param("email") String insertLink);

	public Student findByEmail(String theEmail);
}
