package hr.fer.app.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import hr.fer.app.model.JobAd;

public interface JobAdRepository extends JpaRepository<JobAd, Long>, JpaSpecificationExecutor<JobAd> {

	public List<JobAd> findAll();

	@Modifying
	@Query(value = "insert into JOB_AD_STUDENT_APPLIED (JOB_AD_TO_APPLY_ID,STUDENT_APPLIED_ID)  values (:jobAdId,:studentId)", nativeQuery = true)
	@Transactional
	public void addStudentToJobAdApply(@Param("jobAdId") long jobAdId, @Param("studentId") long studentId);

	@Modifying
	@Query(value = "insert into JOB_AD_STUDENT_FINISHED (FINISHED_JOB_ID,STUDENT_FINISHED_ID)  values (:jobAdId,:studentId)", nativeQuery = true)
	@Transactional
	public void addStudentToJobAdFinished(@Param("jobAdId") long jobAdId, @Param("studentId") long studentId);

}
