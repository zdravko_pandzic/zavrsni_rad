package hr.fer.app.dao;

import java.util.List;

import hr.fer.app.model.City;

public interface DAOInterface {
	public List<City> findAll();
}
