package hr.fer.app.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import hr.fer.app.model.Employer;
import hr.fer.app.model.Review;

public interface ReviewRepository extends JpaRepository<Review, Long> {

	@Query(value = "SELECT r FROM Review r")
	List<Review> findAllByGrade(int i);


}
