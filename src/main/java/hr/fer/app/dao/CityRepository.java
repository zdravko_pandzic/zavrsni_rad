package hr.fer.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.fer.app.model.City;

public interface CityRepository extends JpaRepository<City, Long> {

}
