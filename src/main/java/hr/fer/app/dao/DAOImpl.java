package hr.fer.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import hr.fer.app.model.City;

@Repository
public class DAOImpl implements DAOInterface{
	
	@Autowired
	private EntityManager enitityManager;
	
	@Override
	@Transactional
	public List<City> findAll() {
	
		Session session = enitityManager.unwrap(Session.class);
		Query<City> query = session.createQuery("from City",City.class);

		List<City> list = query.getResultList();
		return list;
	}

}
