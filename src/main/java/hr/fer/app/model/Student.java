package hr.fer.app.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="student")
public class Student implements Serializable {
	
	private static final long serialVersionUID = -2757313383615274669L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private long id;

	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="oib")
	private String oib;

	@Column(name="jmbag")
	private String jmbag;

	@Column(name="email")
	private String email;
	
	@Column(name="password")
	private String password;
	
	@Column(name="phone_number")
	private String phoneNumber;

	@Column(name="enabled")
	private boolean enabled;
	
	//===========relationships
	
	@OneToMany(mappedBy="student")
	private List<Review> studentReviews;

	@ManyToMany(mappedBy="studentFinished")
	private List<JobAd> finishedJob;
	
	
	@ManyToMany(mappedBy="studentApplied")
	private List<JobAd> jobAdToApply;
	//========================
	
	
	public long getId() {
		return id;
	}

	public List<Review> getStudentReviews() {
		return studentReviews;
	}

	public void setStudentReviews(List<Review> studentReviews) {
		this.studentReviews = studentReviews;
	}

	public List<JobAd> getFinishedJob() {
		return finishedJob;
	}

	public void setFinishedJob(List<JobAd> finishedJob) {
		this.finishedJob = finishedJob;
	}

	public List<JobAd> getJobAdToApply() {
		return jobAdToApply;
	}

	public void setJobAdToApply(List<JobAd> jobAdToApply) {
		this.jobAdToApply = jobAdToApply;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getOib() {
		return oib;
	}

	public void setOib(String oib) {
		this.oib = oib;
	}

	public String getJmbag() {
		return jmbag;
	}

	public void setJmbag(String jmbag) {
		this.jmbag = jmbag;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", oib=" + oib + ", jmbag="
				+ jmbag + ", email=" + email + ", password=" + password + ", phoneNumber=" + phoneNumber + ", enabled="
				+ enabled + ", studentReviews=" + studentReviews + ", finishedJob=" + finishedJob + ", jobAdToApply="
				+ jobAdToApply + "]";
	}


}
