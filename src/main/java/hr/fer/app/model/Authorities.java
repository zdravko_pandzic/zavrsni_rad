package hr.fer.app.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "authorities")
public class Authorities implements Serializable {

	private static final long serialVersionUID = -7703795831825991110L;

	
	@Id
	@Column(name = "username")
	private String username;

	@Column(name = "authority")
	private String authority;
	
}
