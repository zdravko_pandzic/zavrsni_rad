package hr.fer.app.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="job_category")
public class JobCategory implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5085924314717915692L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	
	@Column(name="job_category")
	private String jobCategory;
	
	//===========relationships

	@OneToMany(mappedBy="jobCategory")
	private List<JobAd> jobAds;

	//========================
	
	
	public JobCategory() {
		super();
	}
	public JobCategory(long id, String jobCategory) {
		super();
		this.id = id;
		this.jobCategory = jobCategory;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getJobCategory() {
		return jobCategory;
	}
	public void setJobCategory(String jobCategory) {
		this.jobCategory = jobCategory;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((jobCategory == null) ? 0 : jobCategory.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobCategory other = (JobCategory) obj;
		if (id != other.id)
			return false;
		if (jobCategory == null) {
			if (other.jobCategory != null)
				return false;
		} else if (!jobCategory.equals(other.jobCategory))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "JobCategory [id=" + id + ", jobCategory=" + jobCategory + "]";
	}
	
}
