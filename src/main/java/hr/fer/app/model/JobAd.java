package hr.fer.app.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "job_ad")
public class JobAd implements Serializable{
	private static final long serialVersionUID = 2476513686448968357L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@ManyToOne
	private Employer employer;

	@Column(name = "active")
	private boolean active;

	@Column(name = "company_name")
	private String companyName;
	@Column(name = "about_us")
	private String aboutUs;
	@Column(name = "contact_number")
	private String contactNumber;
	@Column(name = "email")
	private String email;

	@Column(name = "address")
	private String address;

	@Column(name = "short_description")
	public String shortDescription;
	@Column(name = "job_description")
	private String jobDescription;
	@Column(name = "salery_per_hour")
	private int saleryPerHour;
	@Column(name = "number_of_students")
	private int numberOfStudents;

	@Column(name = "required_skills")
	private String requiredSkills;
	@Column(name = "required_licenses")
	private String requiredLicenses;
	@Column(name = "minimum_work_experience_year")
	private int minimumWorkExperienceYear;
	@Column(name = "minimum_work_experience_month")
	private int minimumWorkExperienceMonth;

	@Column(name = "date_of_start")
	private LocalDate  dateOfStart;
	@Column(name = "hour_of_start")
	private String hourOfStart;
	@Column(name = "emergency_ad")
	private boolean emergencyAd;

	@Column(name = "date_of_posting")
	private LocalDate  dateOfPosting;



	// ===========relationships

	@OneToMany(mappedBy = "jobAd")
	private List<Review> reviews;

	@ManyToOne
	private City city;

	@ManyToOne
	private JobCategory jobCategory;
	
	 @ManyToMany
	 private List<Student> studentFinished;
	
	@ManyToMany
	private List<Student> studentApplied;
	// ========================

	public void addStudentToStudentFinishedList(Student student) {
		studentFinished.add(student);
		return;
	}
	public void addStudentToStudentAppliedList(Student student) {
		studentApplied.add(student);
		return;
	}
	
	
	public List<Review> getReviews() {
		return reviews;
	}

	public List<Student> getStudentFinished() {
		return studentFinished;
	}

	public void setStudentFinished(List<Student> studentFinished) {
		this.studentFinished = studentFinished;
	}

	public List<Student> getStudentApplied() {
		return studentApplied;
	}

	public void setStudentApplied(List<Student> studentApplied) {
		this.studentApplied = studentApplied;
	}

	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}

	public JobCategory getJobCategory() {
		return jobCategory;
	}

	public void setJobCategory(JobCategory jobCategory) {
		this.jobCategory = jobCategory;
	}

	public List<Student> getStudents() {
		return studentFinished;
	}

	public void setStudents(List<Student> students) {
		this.studentFinished = students;
	}

	public void setDateOfStart(LocalDate dateOfStart) {
		this.dateOfStart = dateOfStart;
	}

	public JobAd() {
		super();
	}

	public JobAd(long id, Employer idEmployer, boolean active, String companyName, String aboutUs, String contactNumber,
			String email, City idCity, String address, String job_description, int saleryPerHour, int numberOfStudents,
			long idJobCategory, String requiredSkills, String requiredLicenses, int minimumWorkExperienceYear,
			int minimumWorkExperienceMonth, LocalDate  dateOfStart, String hourOfStart, boolean emergencyAd) {
		super();
		this.id = id;
		this.employer = idEmployer;
		this.active = active;
		this.companyName = companyName;
		this.aboutUs = aboutUs;
		this.contactNumber = contactNumber;
		this.email = email;
		this.city = idCity;
		this.address = address;
		this.jobDescription = job_description;
		this.saleryPerHour = saleryPerHour;
		this.numberOfStudents = numberOfStudents;
		this.requiredSkills = requiredSkills;
		this.requiredLicenses = requiredLicenses;
		this.minimumWorkExperienceYear = minimumWorkExperienceYear;
		this.minimumWorkExperienceMonth = minimumWorkExperienceMonth;
		this.dateOfStart = dateOfStart;
		this.hourOfStart = hourOfStart;
		this.emergencyAd = emergencyAd;
	}
	
	public LocalDate  getDateOfPosting() {
		return dateOfPosting;
	}

	public void setDateOfPosting(LocalDate dateOfPosting) {
		this.dateOfPosting = dateOfPosting;
	}
	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Employer getEmployer() {
		return employer;
	}

	public void setEmployer(Employer idEmployer) {
		this.employer = idEmployer;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAboutUs() {
		return aboutUs;
	}

	public void setAboutUs(String aboutUs) {
		this.aboutUs = aboutUs;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City idCity) {
		this.city = idCity;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

	public int getSaleryPerHour() {
		return saleryPerHour;
	}

	public void setSaleryPerHour(int saleryPerHour) {
		this.saleryPerHour = saleryPerHour;
	}

	public int getNumberOfStudents() {
		return numberOfStudents;
	}

	public void setNumberOfStudents(int numberOfStudents) {
		this.numberOfStudents = numberOfStudents;
	}

	public String getRequiredSkills() {
		return requiredSkills;
	}

	public void setRequiredSkills(String requiredSkills) {
		this.requiredSkills = requiredSkills;
	}

	public String getRequiredLicenses() {
		return requiredLicenses;
	}

	public void setRequiredLicenses(String requiredLicenses) {
		this.requiredLicenses = requiredLicenses;
	}

	public int getMinimumWorkExperienceYear() {
		return minimumWorkExperienceYear;
	}

	public void setMinimumWorkExperienceYear(int minimumWorkExperienceYear) {
		this.minimumWorkExperienceYear = minimumWorkExperienceYear;
	}

	public int getMinimumWorkExperienceMonth() {
		return minimumWorkExperienceMonth;
	}

	public void setMinimumWorkExperienceMonth(int minimumWorkExperienceMonth) {
		this.minimumWorkExperienceMonth = minimumWorkExperienceMonth;
	}

	public String getDateOfStart() {
		if(dateOfStart!=null) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");			
			return formatter.format(dateOfStart);
		}
		return null;
	}

	public void setDateOfStart(String dateOfStart) {
		if(dateOfStart=="") {
			return;
		}
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate dateTime = LocalDate.parse(dateOfStart, formatter);
		this.dateOfStart = dateTime;
	}

	public String getHourOfStart() {
		return hourOfStart;
	}

	public void setHourOfStart(String hourOfStart) {
		this.hourOfStart = hourOfStart;
	}

	public boolean isEmergencyAd() {
		return emergencyAd;
	}

	public void setEmergencyAd(boolean emergencyAd) {
		this.emergencyAd = emergencyAd;
	}

	@Override
	public String toString() {
		return "JobAd [id=" + id + ", employer=" + employer + ", active=" + active + ", companyName=" + companyName
				+ ", aboutUs=" + aboutUs + ", contactNumber=" + contactNumber + ", email=" + email + ", address="
				+ address + ", shortDescription=" + shortDescription + ", jobDescription=" + jobDescription
				+ ", saleryPerHour=" + saleryPerHour + ", numberOfStudents=" + numberOfStudents + ", requiredSkills="
				+ requiredSkills + ", requiredLicenses=" + requiredLicenses + ", minimumWorkExperienceYear="
				+ minimumWorkExperienceYear + ", minimumWorkExperienceMonth=" + minimumWorkExperienceMonth
				+ ", dateOfStart=" + dateOfStart + ", hourOfStart=" + hourOfStart + ", emergencyAd=" + emergencyAd
				+ ", dateOfPosting=" + dateOfPosting + ", reviews=" + reviews + ", city=" + city + ", jobCategory="
				+ jobCategory + ", studentFinished=" + studentFinished + ", studentApplied=" + studentApplied + "]";
	}

}
