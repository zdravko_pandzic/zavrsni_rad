package hr.fer.app.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


public class JobHistory implements Serializable{
	
	private static final long serialVersionUID = 5846707856389178253L;

	@Id//treba popraviti, ovo su samo 2 strana kljuca
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_student")
	private long idStudent;

	@Column(name="id_ad")	
	private long idAd;
	
	public long getIdStudent() {
		return idStudent;
	}
	public void setIdStudent(long idStudent) {
		this.idStudent = idStudent;
	}
	public long getIdAd() {
		return idAd;
	}
	public void setIdAd(long idAd) {
		this.idAd = idAd;
	}
	
	public JobHistory() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public JobHistory(long idStudent, long idAd) {
		super();
		this.idStudent = idStudent;
		this.idAd = idAd;
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (idAd ^ (idAd >>> 32));
		result = prime * result + (int) (idStudent ^ (idStudent >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobHistory other = (JobHistory) obj;
		if (idAd != other.idAd)
			return false;
		if (idStudent != other.idStudent)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "JobHistory [idStudent=" + idStudent + ", idAd=" + idAd + "]";
	}
}
