package hr.fer.app.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class Users implements Serializable {

	private static final long serialVersionUID = -7156350326560560784L;

	@Id
	@Column(name = "username")
	private String username;

	@Column(name = "enabled")
	private Boolean enabled;

	@Column(name = "password")
	private String password;
	
}
