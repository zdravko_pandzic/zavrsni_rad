package hr.fer.app.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "review")
public class Review implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7861032502803826275L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@ManyToOne
	private Employer employer;

	@ManyToOne
	private Student student;

	@ManyToOne
	private JobAd jobAd;

	@Column(name = "grade")
	private int grade;

	@Column(name = "review_text")
	private String reviewText;

	@Column(name= "whoWroteIt")
	private String whoWroteIt;
	


	public Review() {
		super();
	}

	public Review(long id, Employer employer, Student student, JobAd jobAd, int grade, String reviewText, String whoWroteIt) {
		super();
		this.id = id;
		this.employer = employer;
		this.student = student;
		this.jobAd = jobAd;
		this.grade = grade;
		this.reviewText = reviewText;
		this.whoWroteIt=whoWroteIt;
	}
	
	public String getWhoWroteIt() {
		return whoWroteIt;
	}

	public void setWhoWroteIt(String whoWriteIt) {
		this.whoWroteIt = whoWriteIt;
	}
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Employer getEmployer() {
		return employer;
	}

	public void setEmployer(Employer employer) {
		this.employer = employer;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public JobAd getJobAd() {
		return jobAd;
	}

	public void setJobAd(JobAd jobAd) {
		this.jobAd = jobAd;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getReviewText() {
		return reviewText;
	}

	public void setReviewText(String reviewText) {
		this.reviewText = reviewText;
	}

//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name = "id")
//	private long id;
//
//	@ManyToOne
//	private Student student;
//
//	@ManyToOne
//	private Employer employer;
//
//	@Column(name = "grade")
//	private int grade;
//
//	@Column(name = "review_text")
//	private String reviewText;
//
//	@ManyToOne
//	private JobAd jobAd;

}
