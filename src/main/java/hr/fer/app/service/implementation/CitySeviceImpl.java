package hr.fer.app.service.implementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.app.dao.CityRepository;
import hr.fer.app.model.City;
import hr.fer.app.service.CityService;

@Service
public class CitySeviceImpl implements CityService {

	@Autowired
	private CityRepository cityRepository;
	
	
	@Override
	public List<City> findAll() {
		return cityRepository.findAll();
	}

	@Override
	public City findById(long theId) {
		Optional<City> result = cityRepository.findById(theId);
		
		City theCity = null;
		
		if (result.isPresent()) {
			theCity = result.get();
		}
		else {
			// we didn't find the City
			throw new RuntimeException("Did not find City id - " + theId);
		}
		
		return theCity;
	}

	@Override
	public void save(City theCity) {
		cityRepository.save(theCity);
	}

	@Override
	public void deleteById(long theId) {
		cityRepository.deleteById(theId);
	}
}
