package hr.fer.app.service.implementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.app.dao.StudentRepository;
import hr.fer.app.model.Student;
import hr.fer.app.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService{
	
	@Autowired
	private StudentRepository studentRepository;
	
	
	@Override
	public List<Student> findAll() {
		return studentRepository.findAll();
	}

	@Override
	public Student findById(long theId) {
		Optional<Student> result = studentRepository.findById(theId);
		
		Student theStudent = null;
		
		if (result.isPresent()) {
			theStudent = result.get();
		}
		else {
			// we didn't find the Student
			throw new RuntimeException("Did not find Student id - " + theId);
		}
		
		return theStudent;
	}

	@Override	
	public void save(Student theStudent) {
	    studentRepository.saveInUserTable(theStudent.getEmail(), theStudent.getPassword());
	    studentRepository.saveInAuthoritiesTable(theStudent.getEmail());
	    studentRepository.save(theStudent);
	}

	@Override
	public void deleteById(long theId) {
		studentRepository.deleteById(theId);
	}

	@Override
	public Student findByEmail(String theEmail) {
		return studentRepository.findByEmail(theEmail);
	}
}
