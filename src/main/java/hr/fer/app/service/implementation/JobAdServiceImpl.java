package hr.fer.app.service.implementation;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import hr.fer.app.dao.JobAdRepository;
import hr.fer.app.model.City;
import hr.fer.app.model.JobAd;
import hr.fer.app.service.JobAdService;
import hr.fer.app.service.converter.JobAdSpecification;
import hr.fer.app.service.converter.SearchCriteria;

@Service
public class JobAdServiceImpl implements JobAdService {

	@Autowired
	private JobAdRepository jobAdRepository;

	@Override
	public List<JobAd> findAll() {
		return jobAdRepository.findAll();
	}

	@Override
	public JobAd findById(long theId) {
		Optional<JobAd> result = jobAdRepository.findById(theId);

		JobAd theJobAd = null;

		if (result.isPresent()) {
			theJobAd = result.get();
		} else {
			// we didn't find the JobAd
			throw new RuntimeException("Did not find JobAd id - " + theId);
		}

		return theJobAd;
	}

	@Override
	public void save(JobAd theJobAd) {
		jobAdRepository.save(theJobAd);
	}

	@Override
	public void deleteById(long theId) {
		jobAdRepository.deleteById(theId);
	}

	@Override
	public List<JobAd> function(Date fromDate, Date toDate, Integer salery_per_hour, String date_of_start, City city,
			Pageable pageable) {
		List<JobAd> jobAds = jobAdRepository.findAll(new Specification<JobAd>() {

			@Override
			public Predicate toPredicate(Root<JobAd> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				Predicate p = cb.conjunction();

//				if (Objects.nonNull(fromDate) && Objects.nonNull(toDate) && fromDate.before(toDate)) {
//					p = cb.and(p, cb.between(root.get("date_of_posting"), fromDate, toDate));
//				}
//
//				if (!StringUtils.isEmpty(name)) {
//					p = cb.and(p, cb.like(root.get("name"), "%" + name + "%"));
//				}

				cq.orderBy(cb.desc(root.get("salery_per_hour")), cb.asc(root.get("id")));
				return p;

			}
		}, pageable).getContent();

		return null;// JobAdConverter.convertToJobAdDTO(jobAds);
	}

	@Override
	public Page<JobAd> findAll(Pageable pagable, String saleryPerHour) {

//		if (saleryPerHour != "") {
//
//			JobAdSpecification spec = new JobAdSpecification(new SearchCriteria("saleryPerHour", ">", saleryPerHour));
//			return jobAdRepository.findAll(Specification.where(spec), pagable);
//		}
		return jobAdRepository.findAll(pagable);
	}

	@Override
	public Page<JobAd> findAll(Optional<String> saleryPerHourMax, Optional<String> saleryPerHourMin,
			Optional<String> city, Optional<String> jobCategory, LocalDate dateOfPosting, LocalDate dateOfStart,
			Optional<String> emergencyAd, Optional<String> numberOfStudents, Pageable pagable) {

//		SearchCriteria searchCriteria = new SearchCriteria(saleryPerHourMax, saleryPerHourMin, city, jobCategory,
//				dateOfPosting, dateOfStart, emergencyAd, numberOfStudents);
//
//		JobAdSpecification spec = new JobAdSpecification(searchCriteria);
//
//		return jobAdRepository.findAll(Specification.where(spec), pagable);
		return null;
	}

	@Override
	public void addStudentToJobAdApply(long jobAdId, long studentId) {
		jobAdRepository.addStudentToJobAdApply(jobAdId, studentId);
		return;
	}

	@Override
	public Page<JobAd> findAll(Optional<String> saleryPerHourMax, Optional<String> saleryPerHourMin,
			Optional<String> numberOfStudentsMin, Optional<String> numberOfStudentsMax, LocalDate localDateOfStartMin,
			LocalDate localDateOfStartMax, Optional<String> city, Optional<String> jobCategory,
			Optional<String> emergencyAd, Boolean active, Pageable pagable) {

		SearchCriteria searchCriteria = new SearchCriteria(saleryPerHourMax, saleryPerHourMin, numberOfStudentsMin,
				numberOfStudentsMax, localDateOfStartMin, localDateOfStartMax, city, jobCategory, emergencyAd, active);

		JobAdSpecification spec = new JobAdSpecification(searchCriteria);

		return jobAdRepository.findAll(Specification.where(spec), pagable);
	}

	@Override
	public void addStudentToJobAdFinished(long jobAdId, long studentId) {
		jobAdRepository.addStudentToJobAdFinished(jobAdId, studentId);
		return;
	}

}
