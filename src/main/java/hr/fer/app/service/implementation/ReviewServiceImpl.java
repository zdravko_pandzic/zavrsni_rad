package hr.fer.app.service.implementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.app.dao.ReviewRepository;
import hr.fer.app.model.Employer;
import hr.fer.app.model.Review;
import hr.fer.app.service.ReviewService;

@Service
public class ReviewServiceImpl implements ReviewService{

	@Autowired
	private ReviewRepository reviewRepository;
	
	@Override
	public List<Review> findAll() {
		return reviewRepository.findAll();
	}

	@Override
	public Review findById(long theId) {
		Optional<Review> result = reviewRepository.findById(theId);
		
		Review theReview = null;
		
		if (result.isPresent()) {
			theReview = result.get();
		}
		else {
			// we didn't find the Review
			throw new RuntimeException("Did not find Review id - " + theId);
		}
		
		return theReview;
	}

	@Override	
	public void save(Review theReview) {
		reviewRepository.save(theReview);
	}

	@Override
	public void deleteById(long theId) {
		reviewRepository.deleteById(theId);
	}


}
