package hr.fer.app.service.implementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.app.dao.JobCategoryRepository;
import hr.fer.app.model.JobCategory;
import hr.fer.app.service.JobCategoryService;

@Service
public class JobCategoryServiceImpl implements JobCategoryService {
	
	@Autowired
	private JobCategoryRepository jobCategoryRepository;

	
	
	@Override
	public List<JobCategory> findAll() {
		return jobCategoryRepository.findAll();
	}

	@Override
	public JobCategory findById(long theId) {
		Optional<JobCategory> result = jobCategoryRepository.findById(theId);
		
		JobCategory theJobCategory = null;
		
		if (result.isPresent()) {
			theJobCategory = result.get();
		}
		else {
			// we didn't find the JobCategory
			throw new RuntimeException("Did not find JobCategory id - " + theId);
		}
		
		return theJobCategory;
	}

	@Override	
	public void save(JobCategory theJobCategory) {
		jobCategoryRepository.save(theJobCategory);
	}

	@Override
	public void deleteById(long theId) {
		jobCategoryRepository.deleteById(theId);
	}
}
