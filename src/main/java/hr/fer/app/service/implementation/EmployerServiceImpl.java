package hr.fer.app.service.implementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.app.dao.EmployerRepository;
import hr.fer.app.model.Employer;
import hr.fer.app.service.EmployerService;

@Service
public class EmployerServiceImpl implements EmployerService{

	@Autowired
	private EmployerRepository employerRepository;
	
	
	@Override
	public List<Employer> findAll() {
		return employerRepository.findAll();
	}

	@Override
	public Employer findById(long theId) {
		Optional<Employer> result = employerRepository.findById(theId);
		
		Employer theEmployer = null;
		
		if (result.isPresent()) {
			theEmployer = result.get();
		}
		else {
			// we didn't find the Employer
			throw new RuntimeException("Did not find Employer id - " + theId);
		}
		
		return theEmployer;
	}

	@Override
	public void save(Employer theEmployer) {
		employerRepository.saveInUserTable(theEmployer.getEmail(), theEmployer.getPassword());
		employerRepository.saveInAuthoritiesTable(theEmployer.getEmail());
		employerRepository.save(theEmployer);
	}

	@Override
	public void deleteById(long theId) {
		employerRepository.deleteById(theId);
	}

	@Override
	public Employer findByEmail(String theEmail) {
		return employerRepository.findByEmail(theEmail);
	}
	
	
}
