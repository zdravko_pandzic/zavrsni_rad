//package hr.fer.app.service.implementation;
//
//import java.util.List;
//import java.util.Optional;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import hr.fer.app.dao.JobHistoryRepository;
//import hr.fer.app.model.JobHistory;
//import hr.fer.app.service.JobHistoryService;
//
//@Service
//public class JobHistoryServiceImpl implements JobHistoryService {
//	
//	@Autowired
//	private JobHistoryRepository jobHistoryRepository;
//
//	@Override
//	public List<JobHistory> findAll() {
//		return jobHistoryRepository.findAll();
//	}
//
//	@Override
//	public JobHistory findById(long theId) {
//		Optional<JobHistory> result = jobHistoryRepository.findById(theId);
//		
//		JobHistory theJobHistory = null;
//		
//		if (result.isPresent()) {
//			theJobHistory = result.get();
//		}
//		else {
//			// we didn't find the JobHistory
//			throw new RuntimeException("Did not find JobHistory id - " + theId);
//		}
//		
//		return theJobHistory;
//	}
//
//	@Override	
//	public void save(JobHistory theJobHistory) {
//		jobHistoryRepository.save(theJobHistory);
//	}
//
//	@Override
//	public void deleteById(long theId) {
//		jobHistoryRepository.deleteById(theId);
//	}
//
//
//}
