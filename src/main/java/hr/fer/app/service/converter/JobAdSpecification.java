package hr.fer.app.service.converter;

import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import hr.fer.app.model.JobAd;

public class JobAdSpecification implements Specification<JobAd> {

	private SearchCriteria criteria;

	public JobAdSpecification(SearchCriteria criteria) {
		this.criteria = criteria;
	}

	@Override
    public Predicate toPredicate(Root<JobAd> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
		Predicate p = builder.conjunction();

		p = builder.and(p,builder.equal(root.<Boolean>get("active"),criteria.getActive()));
		
		if(criteria.getSaleryPerHourMin().isPresent()) {
			p = builder.and(p,builder.greaterThanOrEqualTo(root.get("saleryPerHour"), criteria.getSaleryPerHourMin().get()));
		}
		
		if(criteria.getSaleryPerHourMax().isPresent()) {
			p = builder.and(p,builder.lessThanOrEqualTo(root.get("saleryPerHour"), criteria.getSaleryPerHourMax().get()));
		}
		
		if(criteria.getNumberOfStudentsMin().isPresent()) {
			p = builder.and(p,builder.greaterThanOrEqualTo(root.get("numberOfStudents"),criteria.getNumberOfStudentsMin().get()));			
		}
		
		if(criteria.getNumberOfStudentsMax().isPresent()) {
			p = builder.and(p,builder.lessThanOrEqualTo(root.get("numberOfStudents"),criteria.getNumberOfStudentsMax().get()));			
		}
		

		if(Objects.nonNull(criteria.getLocalDateOfStartMin())) {
			LocalDate date = criteria.getLocalDateOfStartMin();
			p = builder.and(p,builder.greaterThan(root.get("dateOfStart"),date));
		}
		 
		if(Objects.nonNull(criteria.getLocalDateOfStartMax())) {
			LocalDate date = criteria.getLocalDateOfStartMax();
			p = builder.and(p,builder.lessThan(root.get("dateOfStart"),date));
		}	

		if(criteria.getCity().isPresent()) {
			p = builder.and(p,builder.equal(root.get("city").get("id"), criteria.getCity().get()));
		}
		
		if(criteria.getJobCategory().isPresent()) {
			p = builder.and(p,builder.equal(root.get("jobCategory").get("id"),criteria.getJobCategory().get()));
		}
		
		if(criteria.getEmergencyAd().isPresent()) {
			boolean bool;
			switch(criteria.getEmergencyAd().get().toString()) {
			case "true":
				bool=true;
				p = builder.and(p,builder.equal(root.<String>get("emergencyAd"),bool));
				break;
			case "false":
				bool=false;
				p = builder.and(p,builder.equal(root.<String>get("emergencyAd"),bool));
				break;
			}
		}
		

		return p;
    }
}