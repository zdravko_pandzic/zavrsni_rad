package hr.fer.app.service.converter;

import java.time.LocalDate;
import java.util.Optional;

public class SearchCriteria {
	private Optional<String> saleryPerHourMax;
	private Optional<String> saleryPerHourMin;
	private Optional<String> numberOfStudentsMin;
	private Optional<String> numberOfStudentsMax;
	private LocalDate localDateOfStartMin;
	private LocalDate localDateOfStartMax;
	private Optional<String> city;
	private Optional<String> jobCategory;
	private Optional<String> emergencyAd;
	private Boolean active;

	public SearchCriteria(Optional<String> saleryPerHourMax, Optional<String> saleryPerHourMin,
			Optional<String> numberOfStudentsMin, Optional<String> numberOfStudentsMax, LocalDate localDateOfStartMin,
			LocalDate localDateOfStartMax, Optional<String> city, Optional<String> jobCategory,
			Optional<String> emergencyAd,Boolean active) {
		super();
		this.saleryPerHourMax = saleryPerHourMax;
		this.saleryPerHourMin = saleryPerHourMin;
		this.numberOfStudentsMin = numberOfStudentsMin;
		this.numberOfStudentsMax = numberOfStudentsMax;
		this.localDateOfStartMin = localDateOfStartMin;
		this.localDateOfStartMax = localDateOfStartMax;
		this.city = city;
		this.jobCategory = jobCategory;
		this.emergencyAd = emergencyAd;
		this.active=active;
	}
	
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Optional<String> getSaleryPerHourMax() {
		return saleryPerHourMax;
	}

	public void setSaleryPerHourMax(Optional<String> saleryPerHourMax) {
		this.saleryPerHourMax = saleryPerHourMax;
	}

	public Optional<String> getSaleryPerHourMin() {
		return saleryPerHourMin;
	}

	public void setSaleryPerHourMin(Optional<String> saleryPerHourMin) {
		this.saleryPerHourMin = saleryPerHourMin;
	}

	public Optional<String> getNumberOfStudentsMin() {
		return numberOfStudentsMin;
	}

	public void setNumberOfStudentsMin(Optional<String> numberOfStudentsMin) {
		this.numberOfStudentsMin = numberOfStudentsMin;
	}

	public Optional<String> getNumberOfStudentsMax() {
		return numberOfStudentsMax;
	}

	public void setNumberOfStudentsMax(Optional<String> numberOfStudentsMax) {
		this.numberOfStudentsMax = numberOfStudentsMax;
	}

	public LocalDate getLocalDateOfStartMin() {
		return localDateOfStartMin;
	}

	public void setLocalDateOfStartMin(LocalDate localDateOfStartMin) {
		this.localDateOfStartMin = localDateOfStartMin;
	}

	public LocalDate getLocalDateOfStartMax() {
		return localDateOfStartMax;
	}

	public void setLocalDateOfStartMax(LocalDate localDateOfStartMax) {
		this.localDateOfStartMax = localDateOfStartMax;
	}

	public Optional<String> getCity() {
		return city;
	}

	public void setCity(Optional<String> city) {
		this.city = city;
	}

	public Optional<String> getJobCategory() {
		return jobCategory;
	}

	public void setJobCategory(Optional<String> jobCategory) {
		this.jobCategory = jobCategory;
	}

	public Optional<String> getEmergencyAd() {
		return emergencyAd;
	}

	public void setEmergencyAd(Optional<String> emergencyAd) {
		this.emergencyAd = emergencyAd;
	}
}
