//package hr.fer.app.service.converter;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.stream.Collectors;
//
//import org.springframework.data.jpa.domain.Specification;
//
//import hr.fer.app.model.JobAd;
//
//public class JobAdSpecificationsBuilder {
//	private List<SearchCriteria> params;
//	
//	public JobAdSpecificationsBuilder() {
//		params= new ArrayList<>();
//	}
//	
//	public JobAdSpecificationsBuilder(String key, String operation, Object value) {
//		params=new ArrayList<>();
//		params.add(new SearchCriteria(key, operation, value));		
//	}
//
//	public Specification<JobAd> build() {
//        if (params.size() == 0) {
//            return null;
//        }
// 
//        List<Specification> specs = params.stream()
//          .map(JobAdSpecification::new)
//          .collect(Collectors.toList());
//         
//        Specification result = specs.get(0);
// 
//        for (int i = 1; i < params.size(); i++) {
//            result = params.get(i)
//              .isOrPredicate()
//                ? Specification.where(result)
//                  .or(specs.get(i))
//                : Specification.where(result)
//                  .and(specs.get(i));
//        }       
//        return result;
//    }
//}
