package hr.fer.app.service;

import java.util.List;

import hr.fer.app.model.City;

public interface CityService {
	public List<City> findAll();
	
	public City findById(long theId);
	
	public void save(City theCity);
	
	public void deleteById(long theId);
		
}
