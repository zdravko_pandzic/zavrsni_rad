package hr.fer.app.service;

import java.util.List;

import hr.fer.app.model.Employer;
import hr.fer.app.model.Review;

public interface ReviewService {
	public List<Review> findAll();
	
	public Review findById(long theId);
	
	public void save(Review theReview);
	
	public void deleteById(long theId);

}
