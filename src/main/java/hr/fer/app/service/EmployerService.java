package hr.fer.app.service;

import java.util.List;

import hr.fer.app.model.Employer;
import hr.fer.app.model.Student;

public interface EmployerService {
	public List<Employer> findAll();
	
	public Employer findById(long theId);
	
	public void save(Employer theEmployer);
	
	public void deleteById(long theId);
	
	public Employer findByEmail(String theEmail);
}
