package hr.fer.app.service;

import java.util.List;

import hr.fer.app.model.Student;

public interface StudentService {
	
	public List<Student> findAll();
	
	public Student findById(long theId);
	
	public void save(Student theStudent);
	
	public void deleteById(long theId);
	
	public Student findByEmail(String theEmail);
	
}
