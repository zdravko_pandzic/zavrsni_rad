package hr.fer.app.service;

import java.util.List;

import hr.fer.app.model.JobCategory;

public interface JobCategoryService {
	public List<JobCategory> findAll();
	
	public JobCategory findById(long theId);
	
	public void save(JobCategory theJobCategory);
	
	public void deleteById(long theId);
}
