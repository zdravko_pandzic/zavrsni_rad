package hr.fer.app.service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import hr.fer.app.model.City;
import hr.fer.app.model.JobAd;

public interface JobAdService {

	public List<JobAd> findAll();
	
	public JobAd findById(long theId);

	public void save(JobAd theJobAd);

	public void deleteById(long theId);

	public List<JobAd> function(Date fromDate, Date toDate, Integer salery_per_hour, String date_of_start, City city,
			Pageable pageable);

	public Page<JobAd> findAll(Pageable pagable, String city);

	public Page<JobAd> findAll(Optional<String> saleryPerHourMax, Optional<String> saleryPerHourMin,
			Optional<String> city, Optional<String> jobCategory, LocalDate dateOfPosting, LocalDate dateOfStart,
			Optional<String> emergencyAd, Optional<String> numberOfStudents, Pageable pageRequest);

	public Page<JobAd> findAll(Optional<String> saleryPerHourMax, Optional<String> saleryPerHourMin,
			Optional<String> numberOfStudentsMin, Optional<String> numberOfStudentsMax, LocalDate localDateOfStartMin,
			LocalDate localDateOfStartMax, Optional<String> city, Optional<String> jobCategory,
			Optional<String> emergencyAd, Boolean active,Pageable pageRequest);
	
	
	public void addStudentToJobAdApply(long jobAdId, long email);
	public void addStudentToJobAdFinished( long jobAdId, long studentId);

}
